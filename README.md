polybar_dotslash_debian 
=======================
polybar install on debian 

*  apt update && apt upgrade

*  apt install sudo git build-essential cmake libcairo2-dev libxcb-randr0-dev alsa-utils libasound2-dev libcurl4-gnutls-dev libmpdclient-dev libiw-dev libpulse-dev xcb-proto libxcb-composite0-dev libxcb-icccm4-dev libxcb-image0-dev libxcb-image0-dev libxcb-ewmh-dev libxcb-util0-dev libxcb-dri2-0-dev python-xcbgen

*  git clone https://github.com/jaagr/polybar.git

*  cd polybar

*  chmod +rx ./build.sh 

*  ./build.sh
